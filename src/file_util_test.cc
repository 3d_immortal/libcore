
#include "file_util.h"

#include "third_party/doctest/doctest/doctest/doctest.h"

namespace core {
namespace {

constexpr char kFilename[] = "/tmp/test.txt";

constexpr char kFileContents[] = "This is a simple\n"
    "file contents\tthat will be tested.\n"
    "to make sure our read write is working.";

// Writes something to a file and reads it back and expects that the contents
// are the same.
TEST_CASE("FileUtilTester.TestReadWriteFiles") {
  CHECK(WriteToFile(kFilename, kFileContents));

  auto read_contents = ReadFile(kFilename);
  CHECK(read_contents);
  CHECK(*read_contents == kFileContents);
}

// Tests that reading from a file that doesn't exist would fail.
TEST_CASE("FileUtilTester.ReadFromANonExistingFile") {
  CHECK_FALSE(ReadFile("random_file.txt"));
}

}  // namespace
}  // namespace core
