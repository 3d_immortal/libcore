
#ifndef CORE_CLOSURE_H_
#define CORE_CLOSURE_H_

#include <functional>

namespace core {

// An alias to a function of type void(void).
using Closure = std::function<void()>;

}  // namespace core

#endif  // CORE_CLOSURE_H_
