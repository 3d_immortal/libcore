
#ifndef CORE_FILE_UTIL_H_
#define CORE_FILE_UTIL_H_

#include <memory>
#include <string>

namespace core {

std::string GetCurrentPath();

// Reads the contents of a file in |file_path| and returns it. |nullptr| is
// returned if it failed to open the file.
std::unique_ptr<std::string> ReadFile(const char* file_path);

// Writes the given |contents| to the file in |file_path|. Returns false on
// failure.
bool WriteToFile(const char* file_path, const std::string& contents);

}  // namespace core

#endif  // CORE_FILE_UTIL_H_
