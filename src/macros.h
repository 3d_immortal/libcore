
#ifndef CORE_MACROS_H_
#define CORE_MACROS_H_

#include <cstdlib>
#include <iostream>
#include <string>

#define DISALLOW_COPY_AND_ASSIGN(type) \
  type(const type& other) = delete;    \
  type& operator=(const type& other) = delete;

#define ERROR(message) \
  std::cerr << std::string("Error: ") << message << std::endl;

#define FATAL_ERROR(message)                                    \
  std::cerr << "[FATAL ERROR: " << __FILE__ << " (" << __LINE__ \
            << ")]: " << message << std::endl;                  \
  std::_Exit(EXIT_FAILURE);

#define CHECK(condition)                         \
  if (!(condition)) {                            \
    FATAL_ERROR("Check failed: " << #condition); \
  }

#ifdef NDEBUG
#define DCHECK(condition) ((void)0)
#else
#define DCHECK(condition) CHECK(condition)
#endif

#endif  // CORE_MACROS_H_
