
#include "file_util.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <algorithm>
#include <fstream>
#include <sstream>

#include "macros.h"

namespace core {

namespace {

constexpr ssize_t kBufferSize = 4096;

}  // namespace

std::string GetCurrentPath() {
  std::string result;
  result.resize(kBufferSize);

  std::stringstream ss;
  ss << "/proc/" << getpid() << "/exe";
  ssize_t bytes = std::min(
      readlink(ss.str().c_str(), const_cast<char*>(result.data()), kBufferSize),
      kBufferSize - 1);
  if (bytes >= 0) {
    result[bytes] = '\0';
    result.resize(result.find_last_of("/"));
  }

  return result;
}

std::unique_ptr<std::string> ReadFile(const char* file_path) {
  std::ifstream file(file_path, std::ios::in | std::ios::binary);
  if (!file) {
    ERROR("Failed to open file: " << file_path);
    return nullptr;
  }

  file.seekg(0, std::ios::end);
  const size_t size = file.tellg();
  file.seekg(0, std::ios::beg);
  auto buffer = std::make_unique<std::string>();
  buffer->resize(size);
  file.read(&((*buffer)[0]), size);

  if (file.bad() || file.fail()) {
    ERROR("An IO error occurred while reading file: " << file_path);
    return nullptr;
  }

  return buffer;
}

bool WriteToFile(const char* file_path, const std::string& contents) {
  std::ofstream file(file_path, std::ios::out | std::ios::binary);
  if (!file) {
    ERROR("Failed to open file: " << file_path);
    return false;
  }

  file.write(contents.data(), contents.size());

  if (file.bad() || file.fail()) {
    ERROR("An IO error occurred while writing to file: " << file_path);
    return false;
  }

  return true;
}

}  // namespace core
