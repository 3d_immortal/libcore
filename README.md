# The Core Library
[![pipeline status](https://gitlab.com/3d_immortal/libcore/badges/master/pipeline.svg)](https://gitlab.com/3d_immortal/libcore/commits/master)
[![coverage report](https://gitlab.com/3d_immortal/libcore/badges/master/coverage.svg)](https://gitlab.com/3d_immortal/libcore/commits/master)

## About
This is a core library which contains foundation functionalities and utilities
that are commonly useful in all my C++ projects.

## Basic Guideline
This library should remain very general, agnostic to any other sub project that
might be using it.

## How To Build

### Prerequisites
The [BUCK](https://buckbuild.com/setup/getting_started.html) build system is
needed in order to build and test this library.

### Build It
Run `make build`

### Test It
Run `make test`

### License
MIT