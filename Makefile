build:
	buck build :core

test:
	buck run @config/gcovr :core_tests -- -fc=true -nv=true
	gcovr --gcov-executable="llvm-cov gcov" -e ".*third_party.*" -r .
	rm -f default.profraw
	
clean:
	buck clean